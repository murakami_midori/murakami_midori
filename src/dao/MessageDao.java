package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("INSERT INTO messages ( ");
			sql.append("user_id");
			sql.append(", subject");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // user_id
			sql.append(", ?"); // category
			sql.append(", ?"); // subject
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getSubject());
			ps.setString(3, message.getText());
			ps.setString(4, message.getCategory());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public class UserMessageDao {

		public List<UserMessage> getUserMessages(Connection connection, int num) {

			PreparedStatement ps = null;
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("SELECT ");
				sql.append("messages.id as id, ");
				sql.append("users.id as user_id, ");
				sql.append("users.name as name, ");
				sql.append("messages.category as category, ");
				sql.append("messages.subject as subject, ");
				sql.append("messages.text as text, ");
				sql.append("messages.created_date as created_date ");
				sql.append("FROM messages ");
				sql.append("INNER JOIN users ");
				sql.append("ON messages.user_id = users.id ");
				sql.append("ORDER BY created_date DESC limit " + num);

				ps = connection.prepareStatement(sql.toString());

				ResultSet rs = ps.executeQuery();
				List<UserMessage> ret = toUserMessageList(rs);
				return ret;
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}

		private List<UserMessage> toUserMessageList(ResultSet rs)
				throws SQLException {

			List<UserMessage> ret = new ArrayList<UserMessage>();
			try {
				while (rs.next()) {
					int id = rs.getInt("id");
					String userId = rs.getString("user_id");
					String name = rs.getString("name");
					String category = rs.getString("category");
					String subject = rs.getString("subject");
					String text = rs.getString("text");
					Timestamp createdDate = rs.getTimestamp("created_date");

					UserMessage message = new UserMessage();
					message.setId(id);
					message.setUserId(userId);
					message.setName(name);
					message.setCategory(category);
					message.setSubject(subject);
					message.setText(text);
					message.setCreatedDate(createdDate);

					ret.add(message);
				}
				return ret;
			} finally {
				close(rs);
			}
		}
	}


	public void deleteMessage(Connection connection, Message message ) {

		PreparedStatement ps = null;

		try{

			String sql = "DELETE FROM messages WHERE id = ? ";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}