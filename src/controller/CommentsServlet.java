package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;;

@WebServlet(urlPatterns = { "/comments" })
public class CommentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		Comment comment = new Comment();

		User user = (User) session.getAttribute("loginUser");

		comment.setUserId(user.getId());
		comment.setMessageId(Integer.parseInt(request.getParameter("message.id")));
		comment.setText(request.getParameter("text"));

		if (isValid(request, messages) == true) {

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {

			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
//	        request.getRequestDispatcher("/top.jsp").forward(request, response);

		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String text = request.getParameter("text");

		if (StringUtils.isBlank(text) == true) {
			messages.add("コメントを入力してください");
		}
		if (500 < text.length() == true) {
			messages.add("コメントは500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}