package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.SettingsService;

@WebServlet(urlPatterns = { "/isWorking" })
public class IsWorkingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		int userId = Integer.parseInt(request.getParameter("userId"));
		int isWrokig = Integer.parseInt(request.getParameter("isWorking"));

		System.out.println(userId);
		System.out.println(isWrokig);

		User user = new SettingsService().isWorking(userId , isWrokig);
		request.setAttribute("user", user);

		response.sendRedirect("userLst");
//		request.getRequestDispatcher("usersLst.jsp").forward(request, response);
	}
}
