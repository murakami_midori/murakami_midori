package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");


		List<String> messages = new ArrayList<String>();

		LoginService loginService = new LoginService();
		User user = loginService.login(loginId, password);

		User loginMissUser = new User();
		loginMissUser.setLoginId(loginId);
		loginMissUser.setPassword(password);


		if(isValid(request, messages) == true){
			if (user != null && user.getIsWorking() != 1) {

				session.setAttribute("loginUser", user);
				response.sendRedirect("./");
			} else {

				messages.add("ログインに失敗しました");
				request.setAttribute("errorMessages", messages);
				request.setAttribute("loginMissUser", loginMissUser);
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}


		} else {

			messages.add("ログインに失敗しました");
			request.setAttribute("errorMessages", messages);
			request.setAttribute("loginMissUser", loginMissUser);

			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");


		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("[0-9a-zA-Z]{6,20}")) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以内で入力してください");
		}
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (!password.matches("[-_@+*;:#$%&A-Za-z0-9]{6,20}")) {
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以内で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}