package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;


@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

        List<Branch> branches = new BranchService().getBranchLst();
        List<Department> departments = new DepartmentService().getDepartmentLst();

        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);

	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

        List<Branch> branches = new BranchService().getBranchLst();
        List<Department> departments = new DepartmentService().getDepartmentLst();

        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);

		List<String> messages = new ArrayList<>();

		HttpSession session = request.getSession();
		String passwordCheck = request.getParameter("passwordCheck");

		User signUpUser = new User();
		signUpUser.setName(request.getParameter("name"));
		signUpUser.setPassword(request.getParameter("password"));
		signUpUser.setLoginId(request.getParameter("loginId"));
		signUpUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		signUpUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		if(isValid(request, messages,passwordCheck) == true) {

			new UserService().register(signUpUser);
			response.sendRedirect("userLst");

		} else {

			messages.add("登録に失敗しました");
			request.setAttribute("signUpUser", signUpUser );
			request.setAttribute(passwordCheck, passwordCheck);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);

		}
	}


	private boolean isValid(HttpServletRequest request, List<String> messages, String passwordCheck) {
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));

	    User user = new UserService().getUserSerch(loginId);

		if(StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if (10 < name.length()) {
			messages.add("名前は10文字以内入力してください");
		}
		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("[0-9a-zA-Z]{6,20}")) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以内で入力してください");
		}
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (!password.matches("[-_@+*;:#$%&A-Za-z0-9]{6,20}")) {
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以内で入力してください");
		}
		if(!password.equals(passwordCheck)) {
			messages.add("パスワードと確認用パスワードが一致していません");
		}
		if(branchId == 0) {
			messages.add("支店名を選択してください");
		}
		if(departmentId == 0) {
			messages.add("部署を選択してください");
		}
		if(branchId != 100 && departmentId == 1 || branchId != 100 && departmentId == 2) {
			messages.add("不正な組み合わせです");
		}
		if(branchId == 100 && departmentId == 3 || branchId == 100 && departmentId == 4) {
			messages.add("不正な組み合わせです");
		}
		if(user != null) {
			messages.add("既に使われているIDです");
		}
			if (messages.size() == 0 ) {
				return true;
			}else {
				return false;
			}
	}
}