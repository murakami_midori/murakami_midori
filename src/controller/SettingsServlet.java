package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.SettingsService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
        List<Branch> branches = new BranchService().getBranchLst();
        List<Department> departments = new DepartmentService().getDepartmentLst();
		List<String> messages = new ArrayList<String>();
//		User loginUser = (User) session.getAttribute("loginUser");

		String checkUserId = (request.getParameter("user.id"));
		System.out.println(checkUserId);
		if(checkUserId != null && checkUserId.length() != 0 && checkUserId.matches("^[0-9]*$")) {


		int getUserId  = Integer.parseInt(checkUserId);
		User user = new UserService().getUser(getUserId);

		if(user != null) {

			User editUser = new SettingsService().setting(getUserId);

		request.setAttribute("editUser", editUser);
        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);

		request.getRequestDispatcher("settings.jsp").forward(request, response);
		} else {
			messages.add("存在しないIDです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userLst");
		}

		} else {
			messages.add("存在しないIDです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userLst");
		}

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
	    String passwordCheck = request.getParameter("passwordCheck");

        List<Branch> branches = new BranchService().getBranchLst();
        List<Department> departments = new DepartmentService().getDepartmentLst();

        request.setAttribute("departments", departments);
        request.setAttribute("branches", branches);

		if (isValid(editUser, messages,passwordCheck) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("editUser", editUser);
			response.sendRedirect("userLst");
		} else {
			session.setAttribute("errorMessages", messages);
			editUser.setBranchId(editUser.getBranchId());
			editUser.setDepartmentId(editUser.getDepartmentId());
			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		editUser.setIsWorking(Integer.parseInt(request.getParameter("isWorking")));
		return editUser;
	}


	private boolean isValid(User editUser, List<String> messages ,String passwordCheck) {

		String loginId = editUser.getLoginId();
		String name = editUser.getName();
		int branchId = editUser.getBranchId();
	    int departmentId = editUser.getDepartmentId();
	    String password = editUser.getPassword();

	    User user = new UserService().getUserSerch(loginId);


		if(StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if (10 < name.length()) {
			messages.add("名前は10文字以内入力してください");
		}
		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("[0-9a-zA-Z]{6,20}")) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以内で入力してください");
		}
		if (!password.equals(passwordCheck)) {
			messages.add("パスワードが確認用パスワードと一致していません");
		}

		if (branchId == 0){
			messages.add("部署を選択してください");
		}
		if (departmentId == 0) {
			messages.add("役職を選択してください");
		}
		if(branchId != 100 && departmentId == 1 || branchId != 100 && departmentId == 2) {
			messages.add("不正な組み合わせです");
		}
		if(branchId == 100 && departmentId == 3 || branchId == 100 && departmentId == 4) {
			messages.add("不正な組み合わせです");
		}
		if(user != null && editUser.getId() != user.getId()) {
			messages.add("既に使われているIDです");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}