package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserLstService;

@WebServlet(urlPatterns = { "/userLst" })
public class UserLstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	 @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

        List<User> users = new UserLstService().getUserLst();
        List<Branch> branches = new BranchService().getBranchLst();
        List<Department> departments = new DepartmentService().getDepartmentLst();

        
        
        request.setAttribute("users", users);
        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);

		request.getRequestDispatcher("usersLst.jsp").forward(request, response);

	}
}