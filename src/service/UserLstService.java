package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;

public class UserLstService {

    public List<User> getUserLst() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> ret = userDao.getUsersLst(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}