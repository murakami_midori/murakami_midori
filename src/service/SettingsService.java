package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.UserDao;

public class SettingsService {

	public User setting(int getUserId) {

		Connection connection = null;
		try {
			connection = getConnection();

			User getuser = new User();

			UserDao userDao = new UserDao();
			getuser = userDao.setting(connection, getUserId);

			commit(connection);

			return getuser;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User isWorking(int userId , int isWrokig) {

		Connection connection = null;

		try {
		connection = getConnection();

		User getuser = new User();

		UserDao userDao = new UserDao();
		userDao.isWorking(connection, userId, isWrokig);

		commit(connection);

			return getuser;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}