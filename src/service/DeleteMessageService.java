package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Message;
import dao.MessageDao;

public class DeleteMessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao MessageDao = new MessageDao();
            MessageDao.deleteMessage(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}