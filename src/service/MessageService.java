package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//    private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage(String start, String end, String category) {

		Connection connection = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String startDay = new String();
		String endDay = new String();

		if(start != null && start.length() > 0) {
			startDay = (start + " 00:00:00");
		} else {
			startDay = "2019-01-01 00:00:00";
		}

		if(end != null && end.length() > 0) {
			endDay = (end + " 23:59:59");
		} else {
			Calendar calendar = Calendar.getInstance();
			endDay = sdf.format(calendar.getTime());
		}


		try {
			connection = getConnection();

			System.out.println("service:" + startDay);
			System.out.println("service:" + endDay);
			System.out.println("service:" + category);

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection, startDay, endDay, category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//	public List<UserMessage> getMessageCategory(String category) {
//
//		Connection connection = null;
//
//		try {
//			connection = getConnection();
//
//			UserMessageDao messageDao = new UserMessageDao();
//			List<UserMessage> ret = messageDao.getUserMessagesCategory(connection, category);
//
//			commit(connection);
//
//			return ret;
//		} catch (RuntimeException e) {
//			rollback(connection);
//			throw e;
//		} catch (Error e) {
//			rollback(connection);
//			throw e;
//		} finally {
//			close(connection);
//		}
//	}
}