package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(filterName="loginFilter", urlPatterns={"/*"})
public class LoginFilter implements Filter {

	public static String INIT_PARAMETER_NAME_ENCODING = "encoding";

	public static String DEFAULT_ENCODING = "UTF-8";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		HttpServletRequest requests = (HttpServletRequest)request;
		HttpServletResponse responses = (HttpServletResponse)response;

		User loginUser = (User)session.getAttribute("loginUser");

		if(!requests.getRequestURI().contains("murakami_midori/css/")){
			if(loginUser == null &&  !requests.getServletPath().equals("/login")) {

				session.setAttribute("loginError", "ログインしてください");
				responses.sendRedirect("login");
				return;
			}
		}
		chain.doFilter(request,response);
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}
