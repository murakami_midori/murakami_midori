<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザ登録</title>
</head>
<body>

	<div class="main-contents">
		<div class="header">
			<ul class="headermenu">
				<c:choose>
					<c:when
						test="${not empty loginUser && loginUser.departmentId == 1}">
						<li><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="userLst">ユーザ一覧</a></li>
						<li><a href="logout">ログアウト</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="logout">ログアウト</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>

		<div class="maincontent">
			<c:if test="${ not empty errorMessages}">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<c:if test="${ signUpUser != null }">
				<form action="signup" method="post">
					<br> <label for="name">名前</label> <input name="name"
						value="${signUpUser.name}" /> <label for="loginId">ログインID</label>
					<input name="loginId" value="${signUpUser.loginId} " /><br> <label
						for="password">パスワード</label> <input name="password"
						type="password" /> <label for="passwordCheck">確認用パスワード</label> <input
						name="passwordCheck" type="password" /> <br>

					<c:if test="${signUpUser.branchId != 0}">
						<label for="branchId">支店</label>
						<select name="branchId">
							<c:forEach items="${branches}" var="branch">
								<c:if test="${signUpUser.branchId == branch.id}">
									<option value="${signUpUser.branchId}" selected>
										<c:out value="${branch.name}" /></option>
								</c:if>
								<c:if test="${signUpUser.branchId != branch.id}">
									<option value="${branch.id}">
										<c:out value="${branch.name}" /></option>
								</c:if>
							</c:forEach>
						</select>
					</c:if>

					<c:if test="${signUpUser.branchId == 0}">
						<label for="branchId">支店</label>
						<select name="branchId">
							<option value="0">支店を選択してください</option>
							<c:forEach items="${branches}" var="branch">
								<option value="${branch.id}"><c:out
										value="${branch.name}" /></option>
							</c:forEach>
						</select>
					</c:if>

					<c:if test="${signUpUser.departmentId != 0}">
						<label for="departmentId">部署</label>
						<select name="departmentId">
							<c:forEach items="${departments}" var="department">
								<c:if test="${signUpUser.departmentId == department.id}">
									<option value="${signUpUser.departmentId}" selected>
										<c:out value="${department.name}" /></option>
								</c:if>
								<c:if test="${signUpUser.departmentId != department.id}">
									<option value="${department.id}">
										<c:out value="${department.name}" /></option>
								</c:if>
							</c:forEach>
						</select>
						<br />
					</c:if>

					<c:if test="${signUpUser.departmentId == 0}">
						<label for="departmentId">部署</label>
						<select name="departmentId">
							<option value="0">部署を選択してください</option>
							<c:forEach items="${departments}" var="department">
								<option value="${department.id}">
									<c:out value="${department.name}" /></option>
							</c:forEach>
						</select>
						<br />
						<br>
					</c:if>

					<input type="submit" value="登録" class="btn" /> <br>
				</form>
			</c:if>

			<c:if test="${signUpUser == null}">
				<form action="signup" method="post">
					<br> <label for="name">名前</label><input name="name" /> <label
						for="loginId">ログインID</label> <input name="loginId" /><br> <label
						for="password">パスワード</label> <input name="password"
						type="password" /> <label for="passwordCheck">確認用パスワード</label> <input
						name="passwordCheck" type="password" /> <br> <label
						for="branchId">支店</label> <select name="branchId">
						<option value="0">支店を選択してください</option>
						<c:forEach items="${branches}" var="branch">
							<option value="${branch.id}"><c:out
									value="${branch.name}" /></option>
						</c:forEach>

					</select> <label for="departmentId">部署</label> <select name="departmentId">
						<option value="0">部署を選択してください</option>
						<c:forEach items="${departments}" var="department">
							<option value="${department.id}">
								<c:out value="${department.name}" /></option>
						</c:forEach>
					</select><br /> <br> <input type="submit" value="登録" class="btn" /> <br>
				</form>
			</c:if>

		</div>
		<div class="copyright">Copyright(c)ALH</div>
	</div>
</body>
</html>