<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>アカウントの設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="main-contents">
		<div class="header">
			<ul class="headermenu">
				<c:choose>
					<c:when
						test="${not empty loginUser && loginUser.departmentId == 1}">
						<li><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="userLst">ユーザ一覧</a></li>
						<li><a href="logout">ログアウト</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="logout">ログアウト</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>

	<div class="maincontent">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">

			<label for="name">名前</label>
			<input name="name"value="${editUser.name}" /><br />
			<label for="loginId">ログインID</label>
			<input name="loginId" value="${editUser.loginId}" /><br />
			<label for="password">パスワード</label>
			<input name="password" type="password" /><br />
			<label for="password">確認用パスワード</label>
			<input name="passwordCheck" type="password" /> <br />

		<c:if test="${loginUser.id != editUser.id}">
			<label for="branchId">支店</label> <select name="branchId">
				<c:forEach items="${branches}" var="branch">
					<c:if test="${editUser.branchId == branch.id}">
						<option value="${editUser.branchId}" selected>
							<c:out value="${branch.name}" /></option>
					</c:if>
					<c:if test="${editUser.branchId != branch.id}">
						<option value="${branch.id}">
							<c:out value="${branch.name}" /></option>
					</c:if>
				</c:forEach>
			</select>
		</c:if>

		<c:if test="${loginUser.id != editUser.id}">
			<label for="departmentId">部署</label> <select name="departmentId">
				<c:forEach items="${departments}" var="department">
					<c:if test="${editUser.departmentId == department.id}">
						<option value="${editUser.departmentId}" selected>
							<c:out value="${department.name}" /></option>
					</c:if>
					<c:if test="${editUser.departmentId != department.id}">
					<option value="${department.id}">
						<c:out value="${department.name}" /></option>
					</c:if>
				</c:forEach>
			</select><br />
		</c:if>

			<input name="isWorking" value="${editUser.isWorking}"type="hidden" />
			<input name="id" value="${editUser.id}" type="hidden" />
			<input type="hidden" name="branchId" value="${editUser.branchId}">
			<input type="hidden" name="departmentId" value="${editUser.departmentId}">

			<input type="submit" value="変更" class="btn2" /> <br />
		</form>

		</div>
		<div class="copyright">Copyright(c)ALH</div>
	</div>
</body>
</html>