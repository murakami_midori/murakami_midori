<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE htmlPUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
</head>
<body>
<div class="main-contents">
	<div class="header">
		<ul class="headermenu">
			<c:choose>
				<c:when test="${not empty loginUser && loginUser.departmentId == 1}">
					<li><a href="./">ホーム</a></li>
					<li><a href="newMessage">新規投稿</a></li>
					<li><a href="userLst">ユーザ一覧</a></li>
					<li><a href="logout">ログアウト</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="./">ホーム</a></li>
					<li><a href="newMessage">新規投稿</a></li>
					<li><a href="logout">ログアウト</a></li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>

	<div class="maincontent">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errormessage">
						<li><c:out value="${errormessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


		<div class="form-area">
			<form action="newMessage" method="post">

				<c:choose>
					<c:when test="${ not empty message}">
						<label for="subject">件名を入力してください</label>
						<input name="subject" type="text" value="${message.subject}" />
						<br />
						<label for="category">カテゴリを入力してください</label>
						<input name="category" type="text" value="${message.category}" />
						<br />本文（1000文字まで）<br />
						<textarea name="text" cols="100" rows="10"><c:out
								value="${message.text}" />
				</textarea>
						<br />
				<input type="submit" value="投稿する" class="btn"><br />
						<br />
					</c:when>
					<c:otherwise>
						<label for="subject">件名（30文字まで）</label>
						<input name="subject" type="text" />
						<br />
						<label for="category">カテゴリ（10文字まで）</label>
						<input name="category" type="text" />
						<br />本文（1000文字まで）<br />
						<textarea name="text" cols="100" rows="10"></textarea>
						<br />
						<input type="submit" value="投稿する" class="btn"><br />
					</c:otherwise>
				</c:choose>

			</form>
		</div>
	</div>
	<div class="copyright">Copyright(c)ALH</div>
</div>
</body>
</html>
