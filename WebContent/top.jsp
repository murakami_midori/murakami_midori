<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE htmlPUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>掲示板</title>
</head>

<body>
	<div class="main-contents">
		<div class="header">
			<ul class="headermenu">
				<c:choose>
					<c:when
						test="${not empty loginUser && loginUser.departmentId == 1}">
						<li><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="userLst">ユーザ一覧</a></li>
						<li><a href="logout">ログアウト</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="logout">ログアウト</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>

		<div class="maincontent">

			<div class="errormessage">
				<c:if test="${not empty accesserror}">
					<div class="accesserror">
						<ul>
							<c:forEach items="${accesserror}" var="accesserror">
								<li><c:out value="${accesserror}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="accesserror" scope="session" />
				</c:if>

				<c:if test="${not empty errorMessages}">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>

				<c:if test="${ not empty accesserror }">
					<div class="accesserror">
						<ul>
							<c:forEach items="${accesserror}" var="accesserror">
								<li><c:out value="${accesserror}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="accesserror" scope="session" />
				</c:if>
			</div>

			<div class="serch">
				<form action="./" method="get">
					<input type="date" name="start" value="${start}" class="dateserch"> ～ <input
						type="date" name="end" value="${end}" class="dateserch"><br /> <input
						type="text" name="category" value="${category}" /> <input
						type="submit" value="検索" class="btn">
				</form>
			</div>

			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="text">
						<div class="subject">
						【<c:out value="${message.subject}" />】</div>
						<p class="postUser">投稿者：<c:out value="${message.name}" /></p>
						<p class="category">カテゴリ：<c:out value="${message.category}" /></p>
						<div class="date">
							<fmt:formatDate value="${message.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
						<div class="maintext">
						<pre><c:out value="${message.text}" /></pre>
						<c:if test="${loginUser.id == message.userId}">
							<form action="deleteMessage" method="post"
								onSubmit="return check()">
								<input type="hidden" name="message.id" value="${message.id}">
								<input type="submit" value="削除" class="btn">
							</form>
						</c:if>
						</div>
					</div>

					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<div class="comment">
								<c:if test="${comment.messageId == message.id}">
									<p class="postUser">投稿者：<c:out value="${comment.name}" /></p>
									<div class="date">
										<fmt:formatDate value="${comment.created_date}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
									<pre><c:out value="${comment.text}" /></pre>
									<c:if test="${loginUser.id == comment.userId}">
										<form action="deleteComment" method="post"
											onSubmit="return check()">
											<input type="hidden" name="comment.id" value="${comment.id}">
											<input type="submit" value="削除" class="btn">
										</form>
									</c:if>
								</c:if>
							</div>
						</c:forEach>


						コメント（500文字まで）
						<form action="comments" method="post">
							<textarea name="text" cols="100" rows="5"></textarea>
							<br /> <input type="hidden" name="message.id"
								value="${message.id}"> <input type="hidden" name="text"
								value="text"> <input type="submit" value="投稿" class="btn">
						</form>
					</div>
				</div>
			</c:forEach>
		</div>
		<div class="copyright">Copyright(c)ALH</div>

		<script>
			function check() {

				if (window.confirm('削除しますか？')) {
					return true;
				} else {
					window.alert('キャンセルされました');
					return false;
				}
			}
		</script>
	</div>
</body>
</html>