<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン</title>
</head>
<body>

	<div class="loginform">
		<h2 class="login-header">LOG IN</h2>

		<div class="login-container">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<c:if test="${ not empty loginError }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${loginError}" var="loginError">
							<li><c:out value="${loginError}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="loginError" scope="session" />
			</c:if>

			<c:choose>
				<c:when test="${loginMissUser != null}">
					<form action="login" method="post">
						<label for="loginId">Login ID</label>
						<input name="loginId" value="${loginMissUser.loginId}" /><br />
						<label for="password">password</label>
						<input name="password" type="password" value="${loginMissUser.password}" /><br />
						<div class="submitButton">
							<input type="submit" class="button" value="Log in" /> <br />
						</div>
					</form>
				</c:when>

				<c:otherwise>
					<form action="login" method="post"><br />
						<label for="loginId">Log in</label>
						<input name="loginId" id="loginId" /> <br />
						<label for="password">password</label>
						<input name="password" type="password"  id="password" /> <br />
						<div class="submitButton">
							<input type="submit" class="button" value="Log in" /> <br />
						</div>
					</form>
				</c:otherwise>
			</c:choose>
			<div class="copyright">Copyright(c)ALH</div>
		</div>
	</div>
</body>
</html>