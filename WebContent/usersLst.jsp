<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザ一覧</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<ul class="headermenu">
				<c:choose>
					<c:when
						test="${not empty loginUser && loginUser.departmentId == 1}">
						<li><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="signup">新規登録</a></li>
						<li><a href="logout">ログアウト</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="logout">ログアウト</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>

		<div class="maincontent">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>


			<c:forEach items="${users}" var="user">
				<div class="user">

					<table border=1px>
						<tr>
							<th>名前</th>
							<td>${user.name}</td>
						</tr>
						<tr>
							<th>ログインID</th>
							<td>${user.loginId}</td>
						</tr>
						<tr>
							<th>支店</th>
							<c:forEach items="${branches}" var="branch">
								<c:if test="${user.branchId == branch.id}">
									<td>${branch.name}</td>
								</c:if>
							</c:forEach>
						</tr>
						<tr>
							<th>部署</th>
							<c:forEach items="${departments}" var="department">
								<c:if test="${user.departmentId == department.id}">
									<td>${department.name}</td>
								</c:if>
							</c:forEach>
						</tr>
						<tr>
							<th>稼動情報</th>
							<td><c:choose>
									<c:when test="${user.isWorking == 0}">稼働中</c:when>
									<c:otherwise>停止中</c:otherwise>
								</c:choose></td>
					</table>

					<script>
						function check() {
							if (window.confirm('変更しますか？')) {
								return true;
							} else {
								window.alert('キャンセルされました');
								return false;
							}
						}
					</script>

					<div class="setting">
					<form action="settings" method="get">
						<input type="hidden" name="user.id" value="${user.id}">
						<input type="submit" value="編集" class="btn">
					</form>
					</div>

					<div class="isWorking">
					<c:if test="${user.id != loginUser.id}">
						<form action="isWorking" method="post" onSubmit="return check()">
							<c:choose>
								<c:when test="${user.isWorking == 0}">
									<button type="submit" name="isWorking" value="1" class="btn">停止する</button>
								</c:when>
								<c:otherwise>
									<button type="submit" name="isWorking" value="0" class="btn">稼働する</button>
								</c:otherwise>
							</c:choose>
							<input type="hidden" name="isWorking" value="${user.id}">
							<input type="hidden" name="userId" value="${user.id}">
						</form>
					</c:if>
					</div>

					<br />
				</div>
			</c:forEach>
		</div>
		<div class="copyright">Copyright(c)ALH</div>
	</div>
</body>
</html>